﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rajabalian.Service;
using Rajabalian.Web.Model;

namespace Rajabalian.Web.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LinksController : Controller
    {
        private readonly IPageService _pageService;
        public LinksController(IPageService pageService)
        {
            this._pageService = pageService;
        }

        [HttpGet("[action]")]
        public IEnumerable<PageViewModel> GetLinks()
        {
            var result = _pageService.GetAllPage();
            return result.Select(s => new PageViewModel { Id = s.Id, Name = s.Name, DisplayName = s.DisplayName });
        }

        [HttpPost("[action]")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SavePage(PageViewModel page)
        {
            try
            {
                await _pageService.SaveAsync(page.ToDomain());
                return Ok("");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetPage(string name)
        {
            return Json(await _pageService.GetPage(name));
        }
    }
}