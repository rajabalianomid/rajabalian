﻿using Rajabalian.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rajabalian.Web.Model
{
    public class PageViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Content { get; set; }

        public Page ToDomain()
        {
            return new Page()
            {
                Content = Content,
                Name = Name,
                DisplayName = DisplayName,
                Id = Id
            };
        }
    }
}
