import { Component } from '@angular/core';
import { DynamiclinksService } from '../services/dynamiclinks.service';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
    isExpanded = false;

    public Links: Link[]
    constructor(private _dynamiclinks: DynamiclinksService) {
        
    }

    ngOnInit() {
        this.LoadLinks();
    }

    collapse() {
        this.isExpanded = false;
    }

    toggle() {
        this.isExpanded = !this.isExpanded;
    }

    LoadLinks(): void {
        this._dynamiclinks.GetAllLinks().subscribe(result => {
            this.Links = result;
        }, error => console.error(error));
    }
}
