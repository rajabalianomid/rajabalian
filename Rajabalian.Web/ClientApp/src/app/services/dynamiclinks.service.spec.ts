import { TestBed, inject } from '@angular/core/testing';

import { DynamiclinksService } from './dynamiclinks.service';

describe('DynamiclinksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DynamiclinksService]
    });
  });

  it('should be created', inject([DynamiclinksService], (service: DynamiclinksService) => {
    expect(service).toBeTruthy();
  }));
});
