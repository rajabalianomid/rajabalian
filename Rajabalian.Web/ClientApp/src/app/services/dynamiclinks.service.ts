import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from '../model/page.model';

@Injectable({
    providedIn: 'root'
})
export class DynamiclinksService {

    public links: Observable<Link[]>;
    constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {

    }

    GetPage(name: string) {
        return this.http.get<string>(this.baseUrl + 'api/v1/links/getpage?name=' + name);
    }

    GetAllLinks() {
        return this.http.get<Link[]>(this.baseUrl + 'api/v1/links/getlinks');
    }

    SavePageHttp(item: Page) {
        return this.http.post(this.baseUrl + 'api/v1/links/SavePage', item)
    }
}
