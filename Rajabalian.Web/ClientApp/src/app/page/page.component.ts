import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DynamiclinksService } from '../services/dynamiclinks.service';

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

    constructor(private route: ActivatedRoute, private _linkService: DynamiclinksService) {
    }
    content: string;
    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            debugger;
            this._linkService.GetPage(params.get(params.keys[0])).subscribe(result => {
                this.content = result;
            }, error => console.error(error));
        });
    }

}
