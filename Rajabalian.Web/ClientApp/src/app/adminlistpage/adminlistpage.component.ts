import { Component, OnInit } from '@angular/core';
import { DynamiclinksService } from '../services/dynamiclinks.service';
import { DBOperation } from '../shared/enums';

@Component({
    selector: 'app-adminlistpage',
    templateUrl: './adminlistpage.component.html',
    styleUrls: ['./adminlistpage.component.css']
})
export class AdminlistpageComponent implements OnInit {

    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;
    isREADONLY: boolean = false;
    public links: Link[];
    public link: Link;
    columns: any[] = [
        {
            display: 'Id',
            variable: 'id',
            filter: 'text',
        },
        {
            display: 'Name',
            variable: 'name',
            filter: 'text'
        },
        {
            display: 'Display Name',
            variable: 'displayName',
            filter: 'text'
        }
    ];
    gridbtns: any[] = [];

    constructor(public _linkService: DynamiclinksService) { }

    ngOnInit() {
        this.LoadLinks();
        this.initGridButton();
    }

    LoadLinks(): void {
        this._linkService.GetAllLinks().subscribe(result => {
            this.links = result;
        }, error => console.error(error));
    }

    editPage(id: number) {
        this.dbops = DBOperation.update;
        this.modalTitle = "Edit User";
        this.modalBtnTitle = "Update";
        this.link = this.links.filter(x => x.id == id)[0];
        alert('Data successfully updated.');
    }
    deletePage(id: number) {
        this.dbops = DBOperation.delete;
        this.modalTitle = "Confirm to Delete?";
        this.modalBtnTitle = "Delete";
        this.link = this.links.filter(x => x.id == id)[0];
        alert('Data successfully deleted.');
    }

    initGridButton() {
        this.gridbtns = [
            {
                title: 'Edit',
                keys: ['Id'],
                action: DBOperation.update,
                ishide: this.isREADONLY
            },
            {
                title: 'X',
                keys: ["Id"],
                action: DBOperation.delete,
                ishide: this.isREADONLY
            }

        ];
    }

    gridaction(gridaction: any): void {

        switch (gridaction.action) {
            case DBOperation.update:
                this.editPage(gridaction.values[0].value);
                break;
            case DBOperation.delete:
                this.deletePage(gridaction.values[0].value);
                break;
        }
    }
}
