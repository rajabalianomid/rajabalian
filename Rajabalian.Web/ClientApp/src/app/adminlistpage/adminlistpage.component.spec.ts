import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminlistpageComponent } from './adminlistpage.component';

describe('AdminlistpageComponent', () => {
  let component: AdminlistpageComponent;
  let fixture: ComponentFixture<AdminlistpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminlistpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminlistpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
