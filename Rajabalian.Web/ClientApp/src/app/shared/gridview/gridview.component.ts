import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

export interface GridAction {
    action: string,
    values: {
        key: string,
        value: string
    }[]
}

@Component({
    selector: 'grid-view',
    templateUrl: './gridview.component.html',
    styleUrls: ['./gridview.component.css']
})
export class GridviewComponent implements OnInit {

    @Output()
    btnclick: EventEmitter<GridAction> = new EventEmitter<GridAction>();

    @Input() columns: any[];
    @Input() data: any[];
    @Input() gridbtns: any[];

    constructor() {
    }

    ngOnInit() {

    }

    click(btn: any, row: any): void {
        let keyds = <GridAction>{};
        keyds.action = btn.action;

        if (row != null) {
            keyds.values = [];
            btn.keys.forEach((key: any) => {
                keyds.values.push({ key: key, value: row[key] });
            });
        }
        this.btnclick.emit(keyds);
    }
}
