import { Component, OnInit } from '@angular/core';
import { Page } from '../model/page.model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { DynamiclinksService } from '../services/dynamiclinks.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debug } from 'util';

@Component({
    selector: 'app-adminpage',
    templateUrl: './adminpage.component.html',
    styleUrls: ['./adminpage.component.css']
})
export class AdminpageComponent implements OnInit {
    public Editor = ClassicEditor;
    page = new Page(null, null, null);
    registerForm: FormGroup;
    submitted = false;

    constructor(public linkService: DynamiclinksService, private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            displayName: ['', Validators.required],
            content: ['']
        });
    }

    onSubmit() {
        this.submitted = true;
        debugger;
        // stop the process here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        //this.page = new Page(this.registerForm.)
        this.sendHttpRequest();
    }

    sendHttpRequest() {
        this.linkService.SavePageHttp(this.page).subscribe(result => {
            var x = result;
            alert('SUCCESS!!');
        }, error => console.error(error));
    }
}
