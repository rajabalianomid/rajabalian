interface Link {
    id: number;
    name: string;
    displayName: string;
    content: string;
}
