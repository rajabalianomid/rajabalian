﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rajabalian.Data;
using Rajabalian.Domain;

namespace Rajabalian.Service
{
    public class PageService : IPageService
    {
        private readonly IRepository<Page> _pageRepository;
        public PageService(IRepository<Page> pageRepository)
        {
            this._pageRepository = pageRepository;
        }

        public List<Page> GetAllPage()
        {
            return _pageRepository.TableNoTracking.ToList();
        }

        public async Task<string> GetPage(string name)
        {
            return await _pageRepository.TableNoTracking.Where(w => w.Name == name.ToLower()).Select(s => s.Content).FirstOrDefaultAsync();
        }
        public async Task RemoveAsync(int id)
        {
            var found = _pageRepository.GetById(id);
            if (found != null)
            {
                await _pageRepository.DeleteAsync(found);
            }
        }
        public async Task SaveAsync(Page page)
        {
            await _pageRepository.InsertAsync(page);
        }
    }
}
