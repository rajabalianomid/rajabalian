﻿using Rajabalian.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rajabalian.Service
{
    public interface IPageService
    {
        List<Page> GetAllPage();
        Task<string> GetPage(string name);
        Task SaveAsync(Page page);
        Task RemoveAsync(int id);
    }
}
