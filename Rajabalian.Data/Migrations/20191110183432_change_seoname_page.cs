﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rajabalian.Data.Migrations
{
    public partial class change_seoname_page : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SeoName",
                table: "Page",
                newName: "DisplayName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DisplayName",
                table: "Page",
                newName: "SeoName");
        }
    }
}
