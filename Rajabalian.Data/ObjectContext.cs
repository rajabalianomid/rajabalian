﻿using Microsoft.EntityFrameworkCore;
using Rajabalian.Data.Mapping;
using Rajabalian.Domain;
using System;

namespace Rajabalian.Data
{
    public class ObjectContext : DbContext, IDbContext
    {
        public ObjectContext(DbContextOptions<ObjectContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(Activator.CreateInstance<PageMap>());
            base.OnModelCreating(modelBuilder);
        }

        public virtual new DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }
        public void Detach<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            var entryEntity = Entry(entity);
            if (entryEntity == null)
                return;

            entryEntity.State = EntityState.Detached;
        }
    }
}
