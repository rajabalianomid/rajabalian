﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Rajabalian.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rajabalian.Data.Mapping
{
    public class PageMap : IEntityTypeConfiguration<Page>
    {
        public void Configure(EntityTypeBuilder<Page> builder)
        {
            builder.HasKey(k => k.Id);
            builder.Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.Property(p => p.DisplayName).IsRequired().HasMaxLength(100);
        }
    }
}
