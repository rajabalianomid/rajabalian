﻿using System;

namespace Rajabalian.Domain
{
    public class Page : BaseEntity, ICloneable
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Content { get; set; }

        public object Clone()
        {
            var page = new Page
            {
                Content = Content,
                Id = Id,
                Name = Name,
                DisplayName = DisplayName
            };
            return page;
        }
    }
}
